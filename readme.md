<u>**Contactless donations - attraction/call to action**</u>

The client, Bristol Culture, are investing in technology to encourage visitors to donate using contactless. They want to test out what sorts of digital displays are most likely to attract a visitor to interact, and follow through with a donation. 

Whilst they don't yet have the donation hardware - they would like to experiment with different interfaces to explore what attracts visitors. 

The scope is therefore open - any multi sensory technology that attracts some kind of interaction. The idea would to then link this to the payment hardware through an api or similar, but for now testing any interaction would be good, so potentially a few different ideas we could test would be ideal.
see here a blog post by National Museums Scotland for inspiration...

<https://medium.com/@cawston/making-contact-digital-experiments-with-visitor-donations-7ed37a757f66>