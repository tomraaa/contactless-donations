# Requirements

**Stakeholders**
1. Bristol Museum. They want the system in their museums
1. Visitors. The people who visit the museum and who will interact with the system display
1. Content Manager. This person will interact with the content management system
1. Curators. They will make content to be displayed on the system
1. Payment Service. They will process all donations for the museum

**Use Case Diagram**

*This section refers to 'usecase.png'*

There are three key actors in the system:
1. Visitors
1. Content Manager
1. Payment Service



**Essential Functional Requirements**

These functional requirements *must* be achieved in order for the system to work.

We have focused on the Content Management System for these requirements.

1. *Upload Content*: The administrators must be able to submit media (videos, text, images) to a CMS

1. *Remove Content*: The administrators must be able to remove any media, that they no longer need or want to use, from the CMS

1. *Link CMS to Interactive Display*: The CMS must link to the Interactive Display so that the media can be viewed

1. *Group/Tag Media*: The admins should be able to tag content to indicate which demographic it is most suitable for

1. *Collect Visitor Email Addresses*: The users should be able to input their email address so that they can subscribe to the museum mailing list

1. *Responsive Media*:
The system must display some exclusive content, such as a thank you message from the curators, when people subscribe to the mailing list

1. *Track Visitor Interactions*: The system should track how many people interact with the display when certain media is playing. The museum should then be able to view this to determine which content works best

**Desirable Functional Requirements**

These requirements would make the system better overall, however are not paramount to it's success.

1. *Edit Content*: It would be good if the Content Manager could edit any text files from within the CMS rather than having to re-upload new Content

1. *Accept Contactless Donations*: The system could handle the contactless payment system, which would also trigger some responsive Media

1. *Select Media*:  The CMS should allow the Content Manager to select from a group of media, rather than uploading the media to display each display

1. *Login System for Content Manager*:  The CMS could have a login for the Content Manager to restrict access and make the system more secure.


**Non Functional Requirements**

1. *Easy to use*: After a brief (1 or 2 hours) training, the administrator should be able to upload, edit, remove, or select content by themselves with no difficulty

1. *Quality of content*: The quality of the content (e.g. the resolution) should be maintained throughout the upload/ display process

1. *Fast loading*: The CMS interface should load quickly (less than 3 seconds), the login process should be efficient, and there should be no buffering on the displays when showing content

1. *Easy to use display*: The interactive display should be able to use from the perspective of a potential donor, no user should be put off due to display being too complicated.

1. *Network independent*: The system could be set up in such a way that it is possible to continue to display content whilst there is no network availability

1. *Fit to screen*: The user interface(s) should be adaptable to different screen sizes, all content should be displayed within the screen dimensions and maximise the use of the space.

 
