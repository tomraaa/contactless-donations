**Stakeholders:**
Bristol Museum - The client, they want the product placed in the venue itself
Donors - The people who will be using the product to donate to the museum
Other visitors - The people who will see but not interact with the product
Curators - The people making the content for the product to display
Staff - People working at the museum
Content manager - The person who selects and uploads the content to be displayed